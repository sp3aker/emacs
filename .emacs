;;;; .emacs --- GNU Emacsin asetustiedosto
;;;; Commentary:
;;;;
;;;; Tässä on listattu yleisemmät asetustiedostot ja niiden sijainnit.
;;;;
;;;; Tässä tiedostossa ei pidä olla mitään ylimääräistä, jotka voivat sotkea
;;;; varsinaiset asetukset.
;;;;
;;;; Code:

;; Varmistetaan että early-init -tiedosto otetaan käyttöön.
(let ((path (expand-file-name "early-init.el" user-emacs-directory)))
  (when (file-exists-p path)
    (require 'early-init path))
  )

;; Jotta vältytään ylimääräistä ongelmilta, niin tallennettaan
;; customize -asetukset omaan tiedostoon.
(setq custom-file "~/.custom.el")
(load "~/.custom.el" t t)

;; Kaikki oleelliset asetukset ovat tässä tiedostossa.
(org-babel-load-file
 (expand-file-name
  "sp3aker-init.org"
  user-emacs-directory))


(provide 'emacs)
;;; .emacs ends here
