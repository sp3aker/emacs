;;;; early-init.el --- Emacsin early-init -tiedosto
;;;; Commentary:

;;;; Code:


;; Käynnistysikkunaan liittyvät asetukset
(setq inhibit-startup-screen t)

;; Ei mitään ilmoitusääntä tai välkkymistä
(setq ring-bell-function 'ignore)

;; Ei sivupalkkia
(setq scroll-bar-mode nil)

(provide 'early-init)
;;; early-init.el ends here
